## Modeling of a4a5b2

For the modeling I am following the instructions and recommendations of this [publication](https://doi.org/10.1371/journal.pcbi.1007449) which is supervising me.

This a piece of art is still in progress :)

### Step 1: Choice of templates

Template selected:

|     ID           | 7KOX    |  7KOO   |  6UWZ   |  PJ's |   7KOQ  |  5KXI |  6PV7   |
|----------------- | ------- | ------- | ------- | ----- | ------- | ----- | ------- |
| Technique        | Cryo-EM | Cryo-EM | Cryo-EM | conf  | Cryo-EM | X-ray | Cryo-EM |
| Resolution Å     |   2.70  |   3.00  |   2.69  |       |   3.60  |  3.94 |  3.34   |
| ICD?             |   YES   |   YES   |   YES   | conf  |   YES   |  NO   |   YES   |
| Ligand           | epib    | a-bgt   |  a-bgt  | conf  | epib    | nico  | nico    |
|                  |PNU120596|         |         |       |         |       |         |
|Detergent/Disc    | disc    | disc    | disc    |       | disc    | det   | disc    |
|Min diameterTMD Å |   7.2   |  9'Leu  | 9'Leu   |       | -1'E    | -1'E  |  -1'E   |
|                  |         |  2.4    |  2.8    |       |  4.3    | 3.8   |  3.4    |
|Composition       | a7      | a7      | a1ya1db1| conf  |   a7    | a4b2  | a3b4    |
|Satete            | open    | closed  | closed  | closed|  des    | des   | des     |

### Step 2: Read the articles of the templates

Publications:

- [5KXI](https://www.nature.com/articles/nature19785) a2b4 publication from 2016. Published by Claudio L. Morales-Perez and Ryan E. Hibbs
- [6PV7](https://doi.org/10.1016/j.neuron.2019.07.030) a3b4 publication from 2019. Published by Anant Gharpure and Ryan E. Hibbs
- [6UWZ](https://doi.org/10.1016/j.neuron.2020.03.012) muscle ach receptor publication from 2020. Published by Mahfuzur Rahman and Ryan E. Hibbs
- [7KOX, 7KOO,  7KOQ](https://doi.org/10.1016/j.cell.2021.02.049) a7 publication from 2021. Published by Colleen M. Noviello and Ryan E. Hibbs

Seems like Ryan E. Hibbs will solve all the acetylcholine receptors soon :)

Notes from publications from oldest to newest:

- **5KXI**

Among our templates, the first acetylcholine receptors that was published was 5KXI. This is the **a4b2 receptor, crystalized with nicotine, an activator, in what seems to be a desensitized state**. To be able to crystalize the structure the authors had to **remove the ICD** and remove the protein form the native membrane environment, i.e. **in detergent**. a4b2 is the most abundant nicotinic receptor in the brain. This receptor can exist in different stoichiometries, being **2a:3b** the one that was crystalized and reported, and also the stoichiometry with a higher affinity to both nicotine and ecytlcholine (~100 fold higher according to the paper). the effects of nicotine in the brain, that lead nicotine consumers to become addicted, is mediated mostly by thereceptor with this stoichiometry. It was observed that after adding nicotine, the receptor decomes desensitized in a few miliseconds. **The desensitized state has a constriction in the transmembrane domain, at the cytosolic end of the pore**. This constriction is formed **by glutamates E in the -1' position of the M2 alpha helices and has a diameter of 3.8 Å**. a4b2 allows the influx of positively charged ions Na+, K+, Ca 2+ (not selective) being K+ the smallest of them. The authors argue that the diameter of the constriction in the E -1' transmembrane part would not even allow the influx of K+ (diameter of 1.9 Å) with an adjacent molecule of wather (diameter of 2.8 Å), this confirms the structure is in a desensitized state. We can visualize the pore and its diameter in Figure 1, description and figure quoted and taken from the original paper, referenced before.

![Figure1](Image/Figure1.png)

Figure 1. Quote: " M2 α-helices from opposing α4 and β2 subunits with side chains shown for pore-lining residues. Blue spheres indicate pore diameters >5.6 Å; yellow are >2.8 Å and < 5.6 Å. c, Pore diameter for the α4β 2 receptor and representative Cys-loop receptors in distinct functional states: desensitized/closed (GABAAR plus benzamidine; Protein Data Bank (PDB) accession 4COF), activated/open (GlyR plus glycine; PDB accession 3JAE) and resting/closed (GlyR plus strychnine; PDB accession 3JAD). Structures were aligned using the M2 helix 9′ leucine, which occurs at y ≈15 Å. The zero value along the y axis in the plot is aligned with the α-carbon of the M2 helix −1′ glutamate residue in α4β2. d, Cutaway of the receptor showing the permeation pathway coloured by electrostatic potential. "

- **6PV7/PV8**

This is the structure of **the desensitized a3b4 receptor** , the most abundant neurotransmitter receptor in the autonomic ganglia. Antagonists of these receptors decrease self-administration of some addictive drugs. The authors kept in both 6PV7 and 6PV8 **most of the ICD** , only removing the disordered loop and stabilizing the receptor with BRIL. 6PV7 was reconstitued into a **lipid nanodisc** using saposin, soy lipids, and a soluble cholesterol derivative. In addition, a monoclonal antibody was complexed to a3 subunits in the receptor, to facilitate cryo-EM studies. The receptor has a **2a:3b stoichiometry** and was resolved **bound to nicotine with a resolution of 3.34 Å**. The reason why I selected 6PV7 and not 6PV8 was because the second could not be resolved in nanodisc and has a low resolution. The authors attempted to resolve 6PV8 with AT1001 bound in the orthosteric site, however the sample aggregated in nanodiscs giving a very low resolution density map (4.6 Å). When purified in detergent the sample improved but there could be conformational differences produced from detergent artifacts. AT1001 is a partial agonist acting as an antagonist by promoting rapid desensitization. This compounds is ~200 times more selective to a3b4 than a4b2. However, classical nicotinic agonists have a 10-100 fold lower affinity to a3b4, compared to a2b4. The higher selectivite of AT1001 to a3b4 might be due to the larger binding site, compared to a4b2,which might accomodate better a larger ligand. Interestingly, AT1001 adopts different poses in the different orthosteric sites. Another important point is that **the pore architecture of 6PV7/PV8 is identical to 5KXI and other previous structures of a4b2. The desensitized pore is constricted at E -1' with a diameter of ~ 3.4 Å**. The upper part of the pore is opened enough to allow the passage of a hydrated sodium ion. We can visualize the pore and its diameter in Figure 2, description and figure quoted and taken from the original paper, referenced before.


![Figure2](Image/Figure2.png)

Figure 2. Quote: "(A) Radius profiles of nicotine and AT-1001 structures colored by hydrophobicity. For clarity, chains A and E are not shown.  (B) Pore radius profiles comparing nicotine, AT-1001, and α4β2 structures and simulations in the presence of nicotine or AT-1001 "

- **6UWZ**

The authors report a high-resolution structure of the **resting muscle-type nAChR**, bound to the **antagonist a-bungarotoxin (a 8 kDa polypeptide) and reconstructed into soy-lipid saposin nanodiscs, at a resolution 2.7 Å** . Part of the toxin goes under loopC, keeping it open, the rest penetretes deeply into the pocket between the a-y and a-d subunits. Its R36 guanidinium group positions in the aromatic box where ACh would bind, thus it would compete with ACh. It also form the classical cation-p interaction between aY198 from loop C and F32 from its own structure. Overall, **the intarections observed by a-bungarotoxin interfere with the binding of ACh and maintains the loop C in an open conformation** . It has been observed that agonists stabilize a compact ECD with a closed loop C, which triggers the oppening of the channel, while antagonists, normally larger, prevent the loop C from closing and the ECD from compacting. If we wanted to describe the permeation state in the pore of the receptor we would begin at the ECD, which is wide and strongly electronegative. In the TMD, the M2 helices from each subunit form the pore, the residues at the 2', 6', 9', 13', 16' and 20' positions face the internal part of the pore. **The hydrophobic LEU in the 9' position located in the middle of the pore, is particularly important to constrict the pore (dmin = 2.8A°)** . This LEU 9' is conserved among other Cys loop receptors. Another important constriction point observed in this structure (NOT observed in 5-HT3 receptors) occurs in the 16' region, near the extracellular end of the pore. Something that is very important to note is that comparing this structure to the closely related 5-HT3 receptor, we observe differences. The first one is the absence of the 16' region and second the presence of a constriction at the base of the pore generated by glutamate side chains. Another important difference form the desensitized state is that the torpedo receptor is widely open at the intracellular end of the pore, as the residues in the -1' position have rotated away. We can visualize the pore and its diameter in Figure 3, description and figure quoted and taken from the original paper, referenced before.

![Figure3](Image/Figure3.png)

Figure 3. Quote: "(A) Axial ion permeation pathway with ribbon representation from three subunits (αγ-γ-αδ). Violet spheres represent solvent-accessible surface.  (B) Permeation pathway cutaway colored by electrostatic potential showing the transition from electronegative to uncharged at the gate and electropositive after the gate.  (C) TMD pore profile from perspective of three pairs of opposing M2 α helices. Minimal diameters are indicated at three positions on the right.  (D) Pore diameter comparison of antagonist-bound Cys-loop receptors (ELIC, PDB: 3RQW; GlyR α3, PDB: 5CFB; GlyR α1, PDB: 3JAD; 5-HT3, PDB: 6HIS; GABAA, PDB: 6HUK). Structures are aligned with y = 0 at the 9′ position.  (E) Pore diameter comparison of toxin-bound Torpedo receptor with apo (PDB: 2BG9) and open state (PDB: 4AQ9) with side chains truncated. Structures are aligned with y = 0 at the 9′ position."

- 7KOX, 7KOO, 7KOQ

This paper describes the structures of the **a7 nicotinic repectors in three conformational states: a closed receptor resolved with the antagonist a-bungarotoxin, an open receptor stabilized by the agonist epibatidine and a positive allosteric modulator (PAM) PNU-120596 and a desensitized structure in complex with epibatidine. They were resolved by cryo-EM in a lipidic-nanodisc.**
This recently published paper is very important of our work because before this all the structures we had for nicotinic receptors were only resolved in closed and desensitized states. The serotonin receptors, which are closely related to nicotinic receptors, lack a structure in a desensitized state and the structures reported in the open state differ significantly from each other. There were only structures in the three different states for the anion-selective glycine receptor. We can visualize the pore and its diameter in Figure 4, description and figure quoted and taken from the original paper, referenced before.

![Figure4](Image/Figure4.png)

Figure 4. Quote: "(A–C) Permeation pathways for the three conformational states—resting (D), activated (E), and desensitized (F)—represented by solid surface colored by hydrophobicity. Constriction points are indicated with distances in Å.  (D–F) Two M2 helices are shown for each of the conformational states—resting (D), activated (E), and desensitized (F)—with side chains shown for pore-lining residues, numbering on the left. Diameters (Å) are indicated with dashed lines."

##### Aminoacid identity in %:
The identity % was calculated on Clustal Omega from the alignment of the complete sequence of each subunit obtained from [UniProtKB](https://www.uniprot.org/uniprot/) in fasta format. This is an improvement from the serotonine templates we were using before, which had an identity of ~27% to a5. (Perhaps I should add here a comparisson to a serotonine receptor?)

|    | a4 | b2 | a3 | b4 | a5 | a7 | a1 | b1 | y  | d  |   
| -  | -  |  - |  - | -  | -  |  - |  - | -  | -  | -  
| a4 | 100| 53 | 58 | 51 | 55 | 40 | 49 | 41 | 35 | 35 |
| b2 | 53 | 100| 49 | 65 | 43 | 39 | 45 | 43 | 41 | 40 |
| a3 | 58 | 49 | 100| 50 | 50 | 39 | 50 | 40 | 38 | 38 |
| b4 | 51 | 65 | 50	| 100| 46 | 39 | 45 | 45 | 41 | 42 |
| a5 | 55 | 43 | 50 | 46 | 100| 37 | 46 | 39 | 33 | 36 |
| a7 | 40 | 39 | 39 | 39 | 37 | 100| 36 | 35 | 29	| 30 |
| a1 | 49 | 45 | 50 | 45 | 46 | 36 | 100| 42 | 35 | 35 |
| b1 | 41 | 43 | 40 | 45 | 39 | 35 | 42 | 100| 42 | 40 |
| y  | 35 | 41 | 38 | 41 | 33 | 29 | 35 | 42	| 100| 49 |
| d  | 35 | 40 | 38 | 42 | 36 | 30 | 35 | 40	| 49 | 100|

### Step 3: Remove unnecessary elements, solvents, ligands, and ions

I did a careful text cleanup of all the PDB files keeping only ATOM, TER and records. This can simplify and facilitate identification of errors in further steps of modeling. 

First load my anaconda environment with [pdb-tools](http://www.bonvinlab.org/pdb-tools/): source activate /c7/home/margonza/miniconda3/envs/molkenv/
With pdb tools, to fetch the pdbs pdb_fetch pdbID > pdbname.pdb
To clean the pdbs the scrip /step3/cleanpdbs.sh with pdb-tools and grep was used

### Step 4: Refine the 3D structure and introduce hydrogens (if missing)

Hydrogens were added first with Maestro and then with [reduce](http://kinemage.biochem.duke.edu/software/reduce.php) explanation [here](http://kinemage.biochem.duke.edu/software/README.reduce.html)
The script is in /step4/addH.sh

### Step 5: Mind the gaps in the alignment
The 3D alignment was done with the output from step4 with VMD. 
The alignment was done by state and by chain A/B/C/D/E

To decide where to put a5 I visualized the ordering of 5kxi and the + vs de - interface. The + interface is the subunit that contains the loopC that will cap the ligand in the pocket. I want a5 to be the + interface to a4 and a4 must continue to be the positive interface to b2.

In 5kxi we have:

```mermaid
graph LR
     a4+A --> b2-B;
     b2-B --> b2-C;
     b2-C --> a4+D
     a4+D --> b2-E;
     b2-E --> a4+A;
```

So in the a4a5b2 model I will substitute the second b2 for a5:

```mermaid
graph LR
     a4+A --> b2-B;
     b2-B --> a5+C;
     a5+C --> a4+D;
     a4+D --> b2-E;
     b2-E --> a4+A;
```

##### Sequence Alignments:

The model and the templates have 5 subunits. To understand how the sequence alignments where done, the following tables depict which subunits were aligned together with the model subunit.

Desensitized:
|  A   |  B   |  C   |  D   |  E   |      |
| ---- | ---- | ---- | ---- | ---- | ---- |
| a3   | b4   | b4   | a3   | b4   | 6pv7 |
| a4   | b2   | b2   | a4   | b2   | 5kxi |
| a7   | a7   | a7   | a7   | a7   | 7koq |
| a4   | b2   | a5   | a4   | b2   | a4a5b2 |

Example: The sequence of the a4 subunit in the model was aligned to the sequences of a7, a4 and a3 on the templates. In the PDBs these are the chain A. The sequence of b2 in the model was aligned with a7, b2 and b4 sequences on the teplates, which are chain B. To model a5, its sequence was aligned with a7, b2 and b4 subunits in the templates, which are chain C. 

Open:
In open I included 5kxi, although it is in desensitized state, just to make sure a4 and b2 were properly aligned.

|  A   |  B   |  C   |  D   |  E   |      |
| ---- | ---- | ---- | ---- | ---- | ---- |
| a4   | b2   | b2   | a4   | b2   | 5kxi |
| a7   | a7   | a7   | a7   | a7   | 7koq |
| a4   | b2   | a5   | a4   | b2   | a4a5b2 |

Closed:
Also included 5kxi here
|  A   |  B   |  C   |  D   |  E   |      |
| ---- | ---- | ---- | ---- | ---- | ---- |
| a4   | b2   | b2   | a4   | b2   | 5kxi |
| a7   | a7   | a7   | a7   | a7   | 7koq |
| a1   | y    | a1   | d    | b1   | 6uwz |
| a4   | b2   | a5   | a4   | b2   | a4a5b2 |

The template structures were aligned with pymol, a file of each chain was created and then aligned with VMD in 3D. In addtion an alignment of the sequences was done using [Clustal Omega](https://www.ebi.ac.uk/Tools/msa/clustalo/) and was compared to the 3D alignment from VMD. The gaps were carefully set based on both sequence and 3D aligments.

To determine which part of the sequences belonged to a4, b2, ICD loop and which part to the MX,MA helices. [Jpre4](http://www.compbio.dundee.ac.uk/jpred4/cgi-bin/jpred_form) was used to predict secondary structures. The HTML files of Jpred4 predictions are in the step5 directory. In all the templates that I am using to model a5, the ICD loop was removed. This loop connects the end of the MX helix to the MA and the M4 helices. See figure 5. Therefore, to construct the models using MODELLER, each chain in the templated had to be truncated. The first part of the chain goes from the beggining of the sequence (ECD) to the end of the MX helix, the second part of the chain goes from the beggining if the MA helix until the end of the M4 helix. In the end each template had 10 chains instead of the initial 5. If the chains are not truncated, then the model would make a very awkward link from the MX helix to the MA in the absence of the ICD loop that links them. This link deforms the MA helix so it had to be avoided. To do this, the chains in the pdbs of the teplates were renamed. Instead of being chain A,B,C,D,E, chain A was divided into chain A and B, chain B beinf MA and M4. The same proccedure was done for the rest of the chains in all the tamplates:

Chains with beggining of ECD to MX: A,C,E,G and I.              Chains with MA and M4: B,D,F,H and J.

To rename the chains in the pdbs of the templates, first I looked at the 3D alignments in VMD and them manually inserted in the PDB chain ID in the first residue where it should beggin. Then, to change chain ID throughout the pdb, the script step5/crop_Chain.py was used.

![Figure5](Image/Figure5.png)

Figure 5. Structure of nicotinic acetylcholine receptor. This figure was modified from this [publication](https://physoc.onlinelibrary.wiley.com/doi/full/10.1113/jphysiol.2009.182691). What I like about this figure is that it shows first a single subunit and the three different domains. In the ICD domain the M3 helix connects to the MX helix. Between the MX and MA helix there is a disordered loop, whose length is different for each subunit type. The MA helix is the intracellular extension of the terminal M4 helix. We then can observe the recepetor with five subunits, three domains and a zoom in to the orthosteric site. The orthosteric site is the place where endogenous ligands bind. In the case of the a4a5b2 receptor, the orthosteric site is located in the interface between the a4 and b2 subunits. The ligand is capped by the lopp c from the principal chain, which is a4. We can also observe the cys-loop in the interface of the EC and TM domains. This loop transduces the conformational changes occurring in the ECD to the TMD by interaction with the loop that links the M2 and M3 helices in the TMD. Finally an upper view of the receptor is shown, this allows to visualize the loopC and the binind interface.

For each state, an initial pir file with the alignments in the format required by MODELLER was created. The alignments were improved iteratively by looking at the models produced by each alignment and the 3D alignment of the models to the templates. The pir files are in /a4a5b2-modeling/step6/closed, /a4a5b2-modeling/step6/open and /a4a5b2-modeling/step6/desensitized directories. 

Overall, most of the differences between the templates were observed in these parts of the structures:
The alpha helix in the N-terminus of the ECD, loop C, MX and MA helix as well as a loop in the ECD, that appears to be longer in the muscle-type nAChR. The MX helix was particularly hard to model, specially for the open state where I only have one template. These differences were visualized in VMD for the a7 experimental structures in open, resting and desensitized state, using the 3D multiseq alignment. Check [this](https://www.ks.uiuc.edu/Training/Tutorials/science/aquaporin/tutorial_aqp-html/node6.html) tutorial if you would like to do this visualization. In Figure 6 you can observe in yellow, the parts of the seuquence that have a structure similarity higher than 0.5 (the maximum is 1) and 0.7. Overall, the structures are well conserved in the ECD domain, within the beta-sandwich core structures. The largest differencer occur in the loop-c, small differences are also observed in the initial alpha-helix and in the loop connecting this helix to the beta-sandwich. In the TMD differences can be obderved in the M2 helix, which is linning the channel pore and whose residues structural changes constrict the pore at different sites in the resting and desensitized states. The M3 and M4 helices maintain fairly similar structures in the closed and desensitized states. The structure of the TMD, and ICD in the open state is different from the other states.

![Figure6](Image/a7-vmdqres.png)

Figure 6. Structural similarity of a7's resting, open and desensitized structures. The plot on the left depicts the Qres values by residue. Residues 1 to ~200 form the ECD, residues 325 to 360 belong to the ICD, which is in between the TMD from residue ~200 to 400. Structural similarity close to 0 occurs in the loop-c, the MX helix and in parts of the MA and M4 helices. On the right the figure shows in yellow the areas of the a7 subunit which have structural similarity values larger than 0.7 and 0.5.

It is important to notice the big differences observed in the MX helix, since these differences are present in the a5 models.
The alignment and VMD visualizations shown in Figures 7 to 10 allowed me to find parts of the model that could be being modeled incorreclty or which had errors in the alignemnt. Whenever I detected residues that different considerably between what was observed for a7, I had a look at the alingment of the model's sequence with the templates for the structural state showing issues and made corrections. The models created in the open state are more challenging, since I only have one template.

![Figure7](Image/a5-qres-vmd.png)

Figure 7. Modeled a5 and structural similarity. Same as before, the plots depict the Qres (structural similarity) values by residue. Residues 1 to ~200 form the ECD, residues 325 to 360 belong to the ICD, which is in between the TMD from residue ~200 to 300. The M4 helix was truncated and to be able to do the alignments on VMD it was not included. Structural similarity close to 0 occurs in the initial residues that are part of the alpha helix at the top of the ECD, and a long loop in the ECD. This was most likely an alignement error that had to be corrected. The MX helix differs significantly, as observed for a7.

![Figure8](Image/a4-qres-VMD.png)

Figure 8. Modeled a4 and tructural similarity. The modeling of a4 shows similar issues as the modeling of a5. Structural similarity close to 0 occurs in the same long loop in the ECD as in a5. This was most likely an alignement error that had to be corrected. The MX helix and the loop-c differ significantly, as observed for a7.

![Figure9](Image/b2-qres.png)

Figure 9. Modeled b2 and structural similarity.  Structural similarity close to 0 occurs in the initial residues that are part of the alpha helix at the top of the ECD. This was most likely an alignement error that had to be corrected.The MX helix differs significantly, as observed for a7.

![Figure10](Image/modelsVMDqres.png)

Figure 10. Modeled a4, b2, a5 and Structural similarity. The figure with structures shows in yellow the areas of the a7 subunit which have structural similarity values larger than 0.7 and 0.5. 

Some issues were observed in the initial helix and loop in the ECD, these were corrected by imposing alpha helix structure on the first 3 residues. Whenever I did not have a template to aling in difficult rigid regions, those residues were substitued from the a4b2 (5KXI) structure after aligning the model to 5kxi. 

All MA and MX helices in the model were also compared to a4b2 and a7, the helix is an helix in all the comparissons and no significant differences were observed.

### Step 6: Consider the size and residue composition of loops during modeling
Here start the modeling part, there are two things I want to do/use: 

First I will use MODELLER to create the raw models, I am using modeller because I believe it is easier to control the symmetry of the chains, important disulfide bonds in adjacent cysteins, trans and cis prolines as well as the structure in the orthosteric site.
Afterwards I will use ProMod3 to model loops, side chains and initial energy minimization.

##### MODELLER
This time I made sure to include ALL the: "oh you should have ..." that I was informed of, nearly a year after. These restrains are in a4a5b2-modeling/step6/closed/c8DOTpir/myModel.py, /a4a5b2-modeling/step6/des/d6DOTpir/myModel.py and /a4a5b2-modeling/step6/open/o10DOTpir/myModel.py. These restraints include:

**1. Include ligand to model the binding pocket.**

Open model:
To create this model we only have one template, a7 7KOX. From this template the agonist epibatidine, in the orhtosteric site, was included in the subunits aligned to a4 and a5 in the model. Including this ligand from the template will help to accomodate the conserved hydrophobic box where ligands bind.

Closed model:
To create this model we have three templates, however two of them the bound ligand is a huge peptide and it would have been complicated to include it as a ligand in MODELLER. Therefore, I used the ligand in PJ's structure, which is confidential.

Desensitized model:
To create this model we have three templates. a4b2 and a3b4 have the same ligand (nicotine) while a7 has epibatidine. I included the ligands of a4b2 and a7.

**2. Make a restraint to insure the proline in the ECD-TMD interface is in cis conformation.** There is no agreement on whether or not this proline should be in cis or trans or its exact role in the gating cycle of the ion channel but we want to keep things similar to what is observed in the a4b2 model (5kxi). When the proline is in cis conformation the omega angle should be close to 0. "The omega (ω) angle in peptide is the torsion angle measured over the peptide bond (source, wikipedia)". To understanf this better have a look at figure 11.

<table align="center"><tr><td align="center" width="9999">
<img src="Image/dihedral.png" align="center" width="450" height="350" >

Figure 11. Dihedral angles in peptides
</td></tr></table>


Comparing the anlge with 5KXI

a4 Chain A angle = -0.2:
PHE 142 CA - PHE 142 C - PRO 143 N - PRO 143 CA

a4 Chain D angle = -0.8:
PHE 142 CA - PHE 142 C - PRO 143 N - PRO 143 CA

b2 Chain B angle = -2.0:
PRO 138 CA - PRO 138 N - PHE 137 C - PHE 137 CA

b2 Chain C angle = -2.3:
PHE 137 CA - PHE 137 C - PRO 138 N - PRO 138 CA

b2 Chain E angle = -2.1:
PHE 137 CA - PHE 137 C - PRO 138 N - PRO 138 CA

I need to indicate MODELLER the residues numbers of the PRO which should be in cis conformation. To do so, first I created an initial model in which the only lines I included in the myModel.py restraints script are to enumerate the residues in each chain starting from 1.

Model a4 Chains A and G: 
PHE 135 CA - PHE 135 C - PRO 136 N - PRO 136 CA

Model a5 Chain E:
PHE 136 CA - PHE 136 C - PRO 137 N - PRO 137 CA 

Model b2 chains C and I:
PHE 137 CA - PHE 137 C - PRO 138 N - PRO 138 CA

**3. Disulfide bonds.** These are in the ECD and one of them is in the loop-c and in the open state when the loopc is closed it might interact with the bound ligand. Note* only alpha subunits have adjacent cysteines in the loop-c.

5kxi a4 chains A and G:
CYS 199 SG to CYS 200 SG,
CYS 149 SG to CYS 135 SG

MODEL a4 chains A and G:
CYS 142 SG to CYS 128 SG,
CYS 192 SG to CYS 193 SG

5kxi b2 chains C and I:
CYS 130 SG to CYS 144 SG

MODEL b2 chains C and I:
CYS 130 SG to CYS 144 SG

MODEL a5 chain E:
CYS 143 SG to CYS 129 SG,
CYS 194 SG to CYS 193 SG

**4. Impose end of MA helix in the open state to be an alpha-helix.**
The template used to model the open state has an incomplete MA helix, so it will have to be modeled as an alpha-helix
Chain B, D, F, H, J residues 1-6

You will find in the OPEN, DES and CLOSED/filesStep6/closedDOTpir_pdbs directories two bash scripts hybridM.sh to produce 100 different models locally (I ran mines in yippy using nohup) and xtract_scores.sh to extract and sort the MODELLER scores to select the best model. The pir alignement in this directories was the final alignment used.

The model with the lowest zDOPE score was selected, I checked that this model fulfilled all the restraints and corrected the badly aligned parts of the model described in the step 5. Corrected models and their alignments are shown in figures 12 to 14. The structural differences observed in the different domains are similar to those observed on the a7 models. In addition a4 and b2 are well aligned to their experimental structures.

![Figure12](Image/a4_QRES.png)
Figure 12. a4 model 

![Figure13](Image/a5-QRES.png)
Figure 13. a5 model

![Figure14](Image/b2-QRES.png)
Figure 14. b2 model

Figures 12 to 13. Show the structural alignemnt and similarity of a4a5b2's resting, open and desensitized structures. The plot  depicts the Qres values by residue. Residues 1 to ~200 form the ECD, residues 325 to 360 belong to the ICD, which is in between the TMD from residue ~200 to 400. Structural similarity close to 0 occurs in the loop-c and the MX helix.

### Step 7: 3D-structure minimization is imperative prior to any computational analysis
### Step 8: Use multiple strategies for 3D structure validation andevaluation
### Step 9: Use a checklist of amino acid protonation states to mimic role of pH
### Step 10: Understand the topologies you use

[Calendar](https://docs.google.com/presentation/d/14rzKthW4TZLbG_YarfyjHi9cR4kg-K8am0Hb692TiOo/edit?usp=sharing)
